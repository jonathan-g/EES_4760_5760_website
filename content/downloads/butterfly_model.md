---
title: "Butterfly Models"
weight: 5
date: "2018-01-23"
due_date: "2018-01-23"
pubdate: "2018-01-01"
descr: "Writing the Butterfly Model (Jan. 23)"
output: html_document
---
# Butterfly Model for Class Exercises on Jan. 23
 
* [Basic Butterfly Model](/models/class_05/butterfly_model_class_5.nlogo) 
  <https://ees4760.jgilligan.org/models/class_05/butterfly_model_class_5.nlogo>
