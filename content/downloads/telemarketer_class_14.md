---
title: "Telemarketer Models"
weight: 12
date: "2018-02-22"
due_date: "2018-02-22"
pubdate: "2018-01-01"
descr: "Telemarketer models (Feb. 22)"
output: html_document
---
# Telemarketer models for in-class exercises

* [class_14_models.zip](/models/class_14/class_14_models.zip), which contains:
  * Testing Is Fun library:
    * [jg-tif.nls](/models/class_14/jg-tif.nls)
  * NetLogo telemarketer models:
    * [telemarketer_class_14.nlogo](/models/class_14/telemarketer_class_14.nlogo)
    * [telemarketer_non_spatial_class_14.nlogo](/models/class_14/telemarketer_class_14.nlogo)
    * [telemarketer_mergers_class_14.nlogo](/models/class_14/telemarketer_class_14.nlogo)
    * [telemarketer_compare_class_14.nlogo](/models/class_14/telemarketer_class_14.nlogo)
  * Behaviorspace Output:
    * [telemarketer_class_14_vary_initial_telemarketers-table.csv](/models/class_14/telemarketer_class_14_vary_initial_telemarketers-table.csv)
    * [telemarketer_non-spatial_class_14_vary_initial_telemarketers-table.csv](/models/class_14/telemarketer_non-spatial_class_14_vary_initial_telemarketers-table.csv)
    * [telemarketer_mergers_class_14_vary_initial_telemarketers-table.csv](/models/class_14/telemarketer_mergers_class_14_vary_initial_telemarketers-table.csv)
    * [telemarketer_compare_class_14_model_comparison-table.csv](/models/class_14/telemarketer_compare_class_14_model_comparisons.csv)
