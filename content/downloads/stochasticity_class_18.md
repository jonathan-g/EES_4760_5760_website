---
title: "Stochasticity: Stochastic Business Investor"
weight: 18
date: "2018-03-21"
due_date: "2018-03-21"
pubdate: "2018-01-01"
descr: "Stochastic business investor model (Mar. 21)"
output: html_document
---
# Stochastic business-investor model for in-class exercise

* [class_18_models.zip](/models/class_18/class_18_models.zip), which contains:
  * Stochastic Business-Investor model: 
    * NetLogo model: [business_investor_class_18.nlogo](/models/class_18/business_investor_class_18.nlogo)
    * Testing library: [jg-tif.nls](/models/class_18/jg-tif.nls)
