---
title: "Marriage Model with Errors"
weight: 7
date: "2018-01-30"
due_date: "2018-01-30"
pubdate: "2018-01-01"
descr: "Marriage Model with Errors (Jan. 30)"
output: html_document
---
# Updated Butterfly Model

* [Butterfly model](/models/class_07/butterfly_class_7.nlogo) 
  for optimizing _q_ to maximize mating
  <https://ees4760.jgilligan.org/models/class_07/butterfly_class_7.nlogo>
* [Elevation data](/models/class_07/ElevationData.txt) for Butterfly model
  <https://ees4760.jgilligan.org/models/class_07/ElevationData.txt>
* [Testing Is Fun](/models/class_07/jg-tif.nls) library:
  <https://ees4760.jgilligan.org/models/class_07/jg-tif.nls>
* [BehaviorSpace output](/models/class_07/butterfly_class_7%20optimize-mating-table.csv)
  for optimizing mating for butterflies starting at the edge of the world:
  <https://ees4760.jgilligan.org/models/class_07/butterfly_class_7%20optimize-mating-table.csv>
* [BehaviorSpace output](/models/class_07/butterfly_class_7%20optimize-mating-big-table.csv)
  for optimizing mating for butterflies starting at different locations:
  <https://ees4760.jgilligan.org/models/class_07/butterfly_class_7%20optimize-mating-big-table.csv>

# Fixing Errors in Models
 
* [ODD for a model of marriage age](/models/class_07/MarriageAgeModel-ODD.pdf) 
  from Chapter 6:<br/>
  <https://ees4760.jgilligan.org/models/class_07/MarriageAgeModel-ODD.pdf>
* A [version of the marriage model](/models/class_07/marriage_model.nlogo)
  with errors:<br/>
  <https://ees4760.jgilligan.org/models/class_07/marriage_model.nlogo>
