---
title: "Team Project Templates"
weight: 11
date: "2018-02-15"
due_date: "2018-01-15"
pubdate: "2018-01-01"
descr: "Templates for team projects (for Feb. 15 class)"
output: html_document
---
# Templates for programming team-project models

## Business Investor Model

* [Printable PDF](/files/odd/business_investor_odd.pdf)
* [Text file](/files/odd/business_investor_odd.md) that you can paste into the "info" tab in your NetLogo model.
* [NetLogo template](/models/team_projects/business_investor_template.nlogo)
* [Testing Is Fun module](/models/team_projects/jg-tif.nls)

## Telemarketer Model:

* [Printable PDF](/files/odd/telemarketer_odd.pdf)
* [Text file](/files/odd/telemarketer_odd.md) that you can paste into the "info" tab in your NetLogo model.
* [Figure for ODD](/files/odd/fig_13_1.md)
* [NetLogo template](/models/team_projects/telemarketer_template.nlogo)
* [Testing Is Fun module](/models/team_projects/jg-tif.nls)
