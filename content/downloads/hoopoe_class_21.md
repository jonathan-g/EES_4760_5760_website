---
title: "Theory Development: Wood Hoopoe Model"
weight: 21
date: "2018-03-27"
due_date: "2018-03-27"
pubdate: "2018-01-01"
descr: "Model of Wood Hoopoe breeding for theory development (Mar. 27)"
output: html_document
---
# Wood Hoopoe breeding models for in-class exercises on theory development

* [class_21_models.zip](/models/class_21/class_21_models.zip), which contains:
  * Wood Hoopoe model: 
    * NetLogo model: [wood_hoopoe_class_21.nlogo](/models/class_21/wood_hoopoe_class_21.nlogo)
    * ODD:  [wood_hoopoe_odd.pdf](/models/class_21/wood_hoopoe_odd.pdf)
    * NetLogo model with alternate strategies: [wood_hoopoe_strategies.nlogo](/models/class_21/wood_hoopoe_strategies.nlogo)
