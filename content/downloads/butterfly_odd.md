---
title: "Butterfly Model ODD"
weight: 4
date: "2018-01-18"
due_date: "2018-01-18"
pubdate: "2018-01-01"
descr: "Documents on ODD protocol (optional supplementary reading for Jan. 18)."
output: html_document
---
## Butterfly Model ODD and Scientific Paper

### Optional supplementary reading for Jan. 18.

* [Netlogo model with Butterfly model ODD on the info page](/models/class_04/butterfly_odd.nlogo).
  I have updated this to work correctly with NetLogo version 6.0.2.
  <https://ees4760.jgilligan.org/models/class_04/butterfly_odd.nlogo>
* G. Pe'er, D. Saltz, and K. Frank, 
  "[Virtual Corridors for Conservation Management](/files/reading/Peer-VirtualCorridors-2005.pdf),"
  _Conservation Biology_, **19**, 1997--2003 (2005).
  <https://ees4760.jgilligan.org/files/reading/Peer-VirtualCorridors-2005.pdf>
  
  This is the journal paper that developed and reported the butterfly model.
  
