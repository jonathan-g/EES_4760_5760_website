---
title: "Artificial Societies"
author: "Peter Tyson"
weight: 2
date: "2018-01-09"
due_date: "2018-01-11"
pubdate: "2018-01-01"
download_link: "/files/reading/Tyson_1997_Artificial_Societies.pdf"
descr: "Reading for Jan. 11."
output: html_document
---
## Reading for Jan. 11

Peter Tyson, "[Artificial Societies]({{% rel_download_url %}})," _MIT Technology Review_ **100**(3), 15--17 (Apr., 1997).
[{{% abs_download_url %}}]({{% abs_download_url %}})
