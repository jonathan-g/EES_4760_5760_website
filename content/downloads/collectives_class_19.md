---
title: "Collectives: Wild-Dog Models"
weight: 19
date: "2018-03-22"
due_date: "2018-03-22"
pubdate: "2018-01-01"
descr: "Models of wild dogs for collectives (Mar. 22)"
output: html_document
---
# Wild-dog models for in-class exercises on collectives

* [class_19_models.zip](/models/class_19/class_19_models.zip), which contains:
  * Wild-dog model: 
    * NetLogo model: [wild_dogs.nlogo](/models/class_19/wild_dogs.nlogo)
    * ODD:  [wild_dog_odd.pdf](/models/class_19/wild_dog_odd.pdf)
  * Testing library:
    * [jg-tif.nls](/models/class_19/jg-tif.nls)
