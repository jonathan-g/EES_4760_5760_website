---
title: "Models for Scheduling"
weight: 13
date: "2018-03-20"
due_date: "2018-03-20"
pubdate: "2018-01-01"
descr: "Models for scheduling (Mar. 20)"
output: html_document
---
# Models for in-class exercises on scheduling

* [class_17_models.zip](/models/class_17/class_17_models.zip), which contains:
  * Mousetrap models:
    * [Mousetrap_Ch14.nlogo](/models/class_17/Mousetrap_Ch14.nlogo)
    * [Mousetrap_Ch14_v2.nlogo](/models/class_17/Mousetrap_Ch14_v2.nlogo)
  * Breeding Synchrony:
    * [Ch_23_4_breeding_synchrony.nlogo](/models/class_23/Ch_23_4_breeding_synchrony.nlogo)
