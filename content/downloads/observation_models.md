---
title: "Butterfly Model for Practicing Observations"
weight: 9
date: "2018-02-13"
due_date: "2018-02-13"
pubdate: "2018-01-01"
descr: "Butterfly Model for Practicing Observations (Feb. 13)"
output: html_document
---
# Modified butterfly  model for practicing observations

* [Basic model](/models/class_10/butterfly_class_10.nlogo)
  <https://ees4760.jgilligan.org/models/class_10/butterfly_class_10.nlogo>
* [Elevation data](/models/class_10/ElevationData.txt)
  <https://ees4760.jgilligan.org/models/class_10/ElevationData.txt>
* [Model with observations fully implemented](/models/class_10/butterfly_class_10_observing.nlogo)
  <https://ees4760.jgilligan.org/models/class_10/butterfly_class_10_observing.nlogo>
