---
title: "ODD Protocol"
author: "Volker Grimm"
weight: 3
date: "2018-01-09"
due_date: "2018-01-18"
pubdate: "2018-01-01"
descr: "Documents on ODD protocol (optional supplementary reading for Jan. 18)."
output: html_document
---
## Documents on ODD protocol

### Optional supplementary reading for Jan. 18.

* The journal article, [V. Grimm _et al._ (2010). "The ODD protocol: A review and first update" _Ecological Modeling_ **221**, 2760--68.](/files/odd/Grimm_2010_ODD_update.pdf). {{% abs_link "/files/odd/Grimm_2010_ODD_update.pdf" %}}
* A Word document that provides [a template for writing ODDs](/files/odd/Grimm_2010_odd_template.docx): {{% abs_link "/files/odd/Grimm_2010_odd_template.docx" %}}
* Lists of scientific publications using agent-based and individual-based models that either do or don't use the ODD protocol (this appeared as [Appendix 1](/files/odd/Grimm_2010_appendix_1.pdf) of the Grimm _et al._ paper):
    * [Appendix 1](/files/odd/Grimm_2010_appendix_1.pdf), {{% abs_link "/files/odd/Grimm_2010_appendix_1.pdf" %}}, 
    * [Publications that did not use ODDs](/files/odd/ch3_ex1_pubs_with_no_ODD.pdf) {{% abs_link "/files/odd/ch3_ex1_pubs_with_no_ODD.pdf" %}}, 
    * [Publications that did use ODDs](/files/odd/ch3_ex2_pubs_with_ODD.pdf) {{% abs_link "/files/odd/ch3_ex2_pubs_with_ODD.pdf" %}}.
