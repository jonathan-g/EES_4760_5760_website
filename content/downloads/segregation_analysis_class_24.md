---
title: "Analyzing and Understanding Models"
weight: 24
date: "2018-03-29"
due_date: "2018-03-29"
pubdate: "2018-01-01"
descr: "Model analysis with Schelling segregation model (Apr. 5)"
output: html_document
---
# Schelling Segregation Model for In-Class Analysis Exercise

* [class_24_models.zip](/models/class_24/class_24_models.zip), which contains:
  * Schelling segregation model: [segregation.nlogo](/models/class_24/segregation.nlogo)
