---
title: "Flocking Model for Studying Emergence"
weight: 8
date: "2018-02-06"
due_date: "2018-02-06"
pubdate: "2018-01-01"
descr: "Flocking model for studying emergence (Feb. 6)"
output: html_document
---
# Modified flocking model for stuying emergence

* [Modified flocking model](/models/class_09/modified_flocking.nlogo)
  <https://ees4760.jgilligan.org/models/class_09/modified_flocking.nlogo>
