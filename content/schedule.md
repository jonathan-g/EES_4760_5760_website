---
date: 2017-04-19
---

#### **Announcement:** Make-up classes to be held Wed. Mar. 21 6:15&ndash;7:30 PM  and Mon. Mar. 26 7:00&ndash;8:00 in Wilson 120 (the Wilson Hall computer classroom).

#### <a href="/assignment/EES_4760_5760_Assignments.pdf" target="_blank"><i class="fa fa-file-pdf-o" style="margin-right:0.25em;"></i> **Printable assignment sheet**</a>

#### <a href="https://www.ees4760.jgilligan.org/projects/"><i class="material-icons">near_me</i> **Individual and Group Project Details**</a>

{{% syllabus %}}
