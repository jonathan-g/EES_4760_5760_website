# Emergence
Jonathan Gilligan  

# Team Projects {#team-project-sec data-transition="fade-out" data-state="skip_slide"}

## Team Projects {#team-projects data-transition="fade-in"}

* For Thursday: Read Chapter 10 and the ODD of the model you will work on. You will spend significant time in class working with your partner(s) to start turning the ODD into a working NetLogo model.

# Emergence {#emergence-sec data-transition="fade-out" data-state="skip_slide"}

## Emergence {#emergence data-transition="fade-in"}

* Download and open the "modified flocking model" from Blackboard (in the NetLogo models folder) or from <https://ees4760.jonathangilligan.org/models/class_09/modified_flocking.nlogo>

* It's easiest if you right-click on the link and choose "Save As" and save the model in a folder on your computer.

* The models we use in this class were mostly written for NetLogo version 5.3.1. 
    * NetLogo 6.0 changed some parts of the NetLogo language, so some of the models won't run correctly in NetLogo 6. 
    * You can install multiple  versions of NetLogo on your computer, but if you do be aware of which version you are running.

## Flocking {.center}

* Play with the model. 
    * Adjust the parameters and see how they change the flocking behavior

## Flocking Model Overview

* Entities:
    * Birds: state-variables `flockmates`, `nearest-neighbor`

* Process:
    * Each bird identifies its `flockmates`
    * Each bird adjusts its direction 
    * Each bird moves forward one patch

## Flocking Model Design Concepts

<div style="text-align:left;">

* Emergence: Large flocks emerge from each bird acting independently, looking only at nearby birds.

</div>

. . .

<div style="text-align:left;">

* Adaptation:
    * If the `nearest-neighbor` is too close, the bird `separates` by turning away from it.
    * Otherwise, the bird:
        1. `aligns`: turns toward its `flockmates`
        2. `coheres`: turns slightly toward the direction the rest of its `flockmates` are flying.

</div>

. . .

<div style="text-align:left;">


* Sensing: The bird can only see a certain distance (`vision`)

</div>

. . .

<div style="text-align:left;">

* Interaction:
    * Each bird interacts with its `flockmates`

</div>

## Submodels

* `find-flockmates`: 
    * `flockmates` are all birds within `vision` distance
    * Alternate interactions:
        * `flockmates` interacts with 6 nearest birds, regardless of distance.
        * Bird only interacts with nearest member of `flockmates`
* `separate`: Turn away from `nearest-neighbor` by up to `max-separate-turn`
* `align`: Turn toward center of `flockmates` by up to `max-align-turn`
* `cohere`: After aligning, turn toward average direction `flockmates` are flying, by up to `max-cohere-turn`


## Observations:

<div style="text-align:left;">

* How to measure flock formation?

</div>

. . .

<div style="text-align:left;">

```
count turtles with [any? flockmates]
mean [count flockmates] of turtles
mean [min [distance myself] of other turtles] of turtles
standard-deviation [heading] of turtles
```

## Digression: Selecting Turtles {.ninety}

* Selection primitives:
    * Returning agent-sets
        * `n-of`, `min-n-of`, `max-n-of`, `other`, 
        * `turtles-on`, `turtles-at`, `turtles-here`, `at-points`
        * `in-radius`, `in-cone`, 
        * `with`, `with-min`, `with-max`
    * Returning individual turtles
        * `one-of`, `min-one-of`, `max-one-of`
        * (may return `nobody`)
    * Look at `Agentset` category in NetLogo dictionary
* Be careful: 
    * Some primitives expect agent-sets
    * Others expect individual turtles.

## Practice Selecting Turtles {.ninety}

* Turn 5 turtles red:

    <div class="fragment">
          
    ```
    ask n-of 5 turtles [ set color red ]
    ```
  
    </div>


* Now for each of those turtles, select all the turtles within a radius of 5 and turn them green

    <div class="fragment">
          
    ```
    ask turtles with [color = red] [ask other turtles in-radius 5 [ set color green ] ]
    ```
  
    </div>

* Now ask each green turtle to calculate the distance to the closest red turtle


    <div class="fragment">
          
    ```
    show [
      min [distance myself] of turtles with [color = red]
      ] of turtles with [color = green]
    ```
  
    </div>

* Now get the average over all the green turtles of the distance to the closest red turtle

    <div class="fragment">
          
    ```
    show mean [
      min [distance myself] of turtles with [color = red]
      ] of turtles with [color = green]          
    ```
  
    </div>


# Experiments  {#experiment-sec data-transition="fade-out" data-state="skip_slide"}

## Experiments  {#experiment data-transition="fade-in"}

* Create a Behaviorspace experiment and call it "Baseline"
    * change one parameter and see how it affects the various measures of flocking.
* Next, duplicate "Baseline" and call it "Flock Type"
    * vary that parameter while also varying the `flock-type`
* Next, duplicate "Baseline" and call it "Multiple"
    * vary more than one parameter (e.g., `vision` and `max-cohere-turn` or `max-align-turn`)

. . .

* Use the `analyze_behaviorspace` app at [https://ees4760.jonathangilligan.org/analyze_behaviorspace](https://ees4760.jonathangilligan.org/analyze_behaviorspace){target="_blank"} to graph the output from your BehaviorSpace experiments.

* Try creating a summary table, saving it to your computer, and opening it in Excel.


