# Science with Models
Jonathan Gilligan  

# Schedule {#schedule-sec .center}

## Schedule {#schedule .center}

* Drop-in model consultation (optional)
    * Wednesday, 5:00--6:00 at my office (SC 5735)
    * Thursday, 5:30--6:30 in this classroom
    * Optional time to drop in and ask questions about your model project
        * Come for as much or as little as you want
        * Bring your computer or leave your model in your Box folder

. . .

* Friday: Drop the current working copy of your NetLogo model in your Box folder before midnight
    * The goal is that your model code is mostly working
    * Then, over the next two weeks:
        * Use this model to perform and analyse behaviorspace experiments
        * In-class presentations and written research report


# Ways to use models {.center}

## Ways to use models {.center}

#. Detailed predictions
#. Theory-building
#. What-if analysis

# Detailed Predictions  {#prediction-sec .center data-transition="fade-out" data-state="skip_slide"}

## Detailed Predictions  {#prediction .center data-transition="fade-in"}

* Develop model:
    * Theory-driven 
        * Use existing theory
    * Data-driven
        * Need _lots_ of observations/data
        * Look for patterns in data
        * Describe patterns mathematically
* Calibrate model
    * Mathematical theory has parameters
        * Adjust parameters to make model agree with past observations
* Validate & Verify

## Validation & Verification {.center}

* __Cross-validation__ (for comparing theories):
    * Divide data into $k$ parts: $1 \ldots k$
        * Each part has a turn as ``test set'':
            * Fit model parameters to the other parts
            * Compare predictions to test set.
    * Choose model that performs best over the $k$ comparisons.

. . .    
    
* __Hold-out testing__ (for estimating predictive accuracy):
    * Divide data into _hold-out_ and _training_ data:
        * Divide _training_ data into $k$ parts
        * Use cross-validation to choose best model
        * Calibrate best model on full _training_ set
        * Test predictions against _hold-out_ set to estimate predictive power


# Theory-Building {#theory-building-sec .center data-transition="fade-out" data-state="skip_slide"}

## Theory-Building {#theory-building .center data-transition="fade-in"}

* Similar to detailed prediction
    * Detailed prediction often uses very complicated models to capture all the
      relevant details of the real world
    * Theory-building often uses simplified models to capture just the most
      important aspects of what makes the real-world system tick.

# What-if analysis {#what-if-sec .center data-transition="fade-out" data-state="skip_slide"}

## What-if analysis {#what-if .center data-transition="fade-in"}

> * Does not necessarily need data
> * Start with simple theory or hypothesis
> * Explore implications

## Robust Policy Analysis

* How to make policy under extreme uncertainty:
    * R. Lempert: Making policy for the next 100 years
        * planning for climate change, technological revolutions, etc.
    * You can't predict what will happen
    * Division: _policy variables_ (things you can control), _external variables_ (things you can't control).
    * Use ___lots___ of model runs (BehaviorSpace goes nuts)
        * Which sets of _policy variables_ avoid catastrophic outcomes across the widest range of _external variables_?

# Realistic Expectations {.center}

## Realistic Expectations {.center}

* Most ABM work ___does not___ aspire to make detailed predictions
* Much focuses on either theory-building or what-if analysis

## Let's talk about your modeling projects {.center}

* What are you hoping to do with your model?
* What difficulties are you having?
